/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Polygon} from 'geojson';
import {SCSetting} from '../../things/Setting';
import {SCTranslations} from '../i18n';

/**
 * An app configuration menu item
 */
export interface SCAppConfigurationMenuItem {
  /**
   * Icon for the menu item
   */
  icon: string;

  /**
   * Route inside the app
   */
  route: string;

  /**
   * Title of the route
   */
  title: string;

  /**
   * Translations for the menu item
   */
  translations: SCTranslations<SCAppConfigurationMenuItemTranslationTitle>;
}

/**
 * An app configuration menu category
 */
export interface SCAppConfigurationMenuCategory {
  /**
   * Icon for the menu category
   */
  icon: string;

  /**
   * ID of the menu category
   */
  id:
    'main'
    | 'meta'
    | 'personal'
    | 'external';

  /**
   * A list of items that belong to the category
   */
  items: SCAppConfigurationMenuItem[];

  /**
   * Name of the category
   */
  name: string;

  /**
   * Translations for the menu category
   */
  translations: SCTranslations<SCAppConfigurationMenuCategoryTranslationName>;
}

/**
 * An app configuration
 */
export interface SCAppConfiguration {
  /**
   * Polygon that encapsulates the main campus
   */
  campusPolygon: Polygon;

  /**
   * A list of features to en- or disable
   */
  features: SCAppConfigurationFeature;

  /**
   * A URL where images are available
   */
  imageUrl?: string;

  /**
   * A list of available menu categories in the app
   */
  menus: SCAppConfigurationMenuCategory[];

  /**
   * Name for the app
   */
  name: string;

  /**
   * URL to a file containing the privacy policy
   */
  privacyPolicyUrl: string;

  /**
   * A list of available settings in the app
   */
  settings: SCSetting[];

  /**
   * Map of store URLs
   */
  storeUrl?: SCAppConfigurationStoreUrl;

  /**
   * URL where a web instance of the app is available
   */
  url?: string;
}

/**
 * 
 */
export interface SCAppConfigurationFeature {
  /**
   * 
   */
  widgets: boolean;
}

/**
 * URLs of published apps
 */
export interface SCAppConfigurationStoreUrl {
  /**
   * Google Play Store URL
   */
  android?: string;
  /**
   * Apple App Store URL
   */
  ios?: string;
  /**
   * Microsoft Store URL 
   */
  uwp?: string;
}

/**
 * Translatable property of a menu item
 */
export interface SCAppConfigurationMenuItemTranslationTitle {
  /**
   * Translation of the title of a menu item
   */
  title: string;
}

/**
 * Translatable property of a menu category
 */
export interface SCAppConfigurationMenuCategoryTranslationName {
  /**
   * Translation of the name of a menu category
   */
  name: string;
}
