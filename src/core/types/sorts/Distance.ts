/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Position} from 'geojson';
import {SCSearchAbstractSort, SCSearchAbstractSortArguments} from './Abstract';

/**
 * Sort instruction to sort by distance
 */
export interface SCDistanceSort extends SCSearchAbstractSort<SCDistanceSortArguments> {
  type: 'distance';
}

/**
 * Additional arguments for sort instruction to sort by distance
 */
export interface SCDistanceSortArguments extends SCSearchAbstractSortArguments {
  /**
   * Position to calculate distances to
   */
  position: Position;
}
