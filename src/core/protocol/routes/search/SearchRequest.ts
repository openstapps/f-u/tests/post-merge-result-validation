/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAbstractRoute, SCRouteHttpVerbs} from '../../../Route';
import {SCSearchContext} from '../../../types/config/Backend';
import {SCSearchFilter} from '../../../types/filters/Abstract';
import {SCSearchSort} from '../../../types/sorts/Abstract';
import {
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
  SCUnsupportedMediaTypeErrorResponse,
  SCValidationErrorResponse,
} from '../../errors/ErrorResponse';

/**
 * A search request
 *
 * @validatable
 */
export interface SCSearchRequest extends SCSearchQuery {
}

/**
 * A search query
 */
export interface SCSearchQuery {
  /**
   * The context name from where the search query was initiated
   */
  context?: SCSearchContext;

  /**
   * A filter structure that combines any number of filters with boolean methods ('AND', 'OR', 'NOT')
   */
  filter?: SCSearchFilter;

  /**
   * Number of things to skip in result set (paging)
   */
  from?: number;

  /**
   * A term to search for
   */
  query?: string;

  /**
   * Number of things to have in the result set (paging)
   */
  size?: number;

  /**
   * A list of sorting parameters to order the result set by
   */
  sort?: SCSearchSort[];
}

/**
 * Route for searching things
 */
export class SCSearchRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCSearchRequest';
    this.responseBodyName = 'SCSearchResponse';
    this.statusCodeSuccess = 200;
    this.urlFragment = '/search';
  }
}
