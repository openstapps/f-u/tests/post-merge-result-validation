/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThings, SCThingsField} from '../../../Classes';

/**
 * A search response
 *
 * @validatable
 */
export interface SCSearchResponse extends SCSearchResult {
}

/**
 * A search response
 */
export interface SCSearchResult {
  /**
   * Data (any data object)
   */
  data: SCThings[];

  /**
   * Facets (aggregations over all matching data)
   */
  facets: SCFacet[];

  /**
   * Pagination information
   */
  pagination: SCSearchResponsePagination;

  /**
   * Stats of the search engine
   */
  stats: SCSearchResponseSearchEngineStats;
}

/**
 * A search facet
 */
export interface SCFacet {
  /**
   * Buckets for the aggregation
   */
  buckets: SCFacetBucket[];

  /**
   * Field of the aggregation
   */
  field: SCThingsField;
}

/**
 * A bucket of a facet
 */
export interface SCFacetBucket {
  /**
   * Count of matching search results
   */
  count: number;

  /**
   * Key of a bucket
   */
  key: string;
}

/**
 * Stores information about Pagination
 */
export interface SCSearchResponsePagination {
  /**
   * Count of given data. Same as data.length
   */
  count: number;

  /**
   * Offset of data on all matching data. Given by [[SCSearchQuery.from]]
   */
  offset: number;

  /**
   * Number of total matching data
   */
  total: number;
}

/**
 * Statistics of search engine
 */
export interface SCSearchResponseSearchEngineStats {
  /**
   * Response time of the search engine in ms
   */
  time: number;
}
