/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAbstractRoute, SCRouteHttpVerbs} from '../../../Route';
import {SCMap} from '../../../types/Map';
import {
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
  SCTooManyRequestsErrorResponse,
  SCUnsupportedMediaTypeErrorResponse,
  SCValidationErrorResponse,
} from '../../errors/ErrorResponse';
import {SCSearchQuery} from './SearchRequest';

/**
 * A multi search request
 *
 * This is a map of [[SCSearchRequest]]s indexed by name.
 *
 * **CAUTION: This is limited to an amount of queries. Currently this limit is 5.**
 *
 * @validatable
 */
export type SCMultiSearchRequest = SCMap<SCSearchQuery>;

/**
 * Route for submission of multiple search requests at once
 */
export class SCMultiSearchRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCTooManyRequestsErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCMultiSearchRequest';
    this.responseBodyName = 'SCMultiSearchResponse';
    this.statusCodeSuccess = 200;
    this.urlFragment = '/search/multi';
  }
}
