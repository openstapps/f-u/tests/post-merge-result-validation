/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Schema} from 'jsonschema';
import {SCAbstractRoute, SCRouteHttpVerbs} from '../../../Route';
import {
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse, SCParametersNotAcceptable,
  SCPluginAlreadyRegisteredErrorResponse,
  SCPluginRegisteringFailedErrorResponse,
  SCPluginRouteAlreadyRegisteredErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
} from '../../errors/ErrorResponse';

/**
 * Plugin register request
 *
 * @validatable
 */
export type SCPluginRegisterRequest = AddPlugin | RemovePlugin;
interface AddPlugin {
  /**
   * The desired action, so whether the plugin should be added or removed
   */
  action: 'add';

  /**
   * The address of the plugin
   */
  address: string;

  /**
   * The name of the plugin
   * Just for debugging purposes, to more easily identify conflicts.
   */
  name: string;

  /**
   * How the requests of the plugin looks like, a JSON schema for validation
   */
  pluginRequestSchema: Schema;

  /**
   * How the responses of the plugin looks like, a JSON schema for validation
   */
  pluginResponseSchema: Schema;

  /**
   * The desired route, for example /feedback.
   */
  route: string;
}
interface RemovePlugin {
  /**
   * The desired action, so whether the plugin should be added or removed
   */
  action: 'remove';

  /**
   * The route of the plugin you want to remove
   */
  route: string;
}

/**
 * Route to register plugins
 */
export class SCPluginRegisterRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCParametersNotAcceptable,
      SCPluginAlreadyRegisteredErrorResponse,
      SCPluginRouteAlreadyRegisteredErrorResponse,
      SCPluginRegisteringFailedErrorResponse,
      SCPluginRouteAlreadyRegisteredErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCPluginRegisterRequest';
    this.responseBodyName = 'SCPluginRegisterResponse';
    this.statusCodeSuccess = 200;
    this.urlFragment = '/plugin/register';
  }
}
