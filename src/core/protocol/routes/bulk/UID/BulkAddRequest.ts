/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThings} from '../../../../Classes';
import {SCAbstractRoute, SCRouteHttpVerbs} from '../../../../Route';
import {
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse,
  SCNotFoundErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
  SCUnsupportedMediaTypeErrorResponse,
  SCValidationErrorResponse,
} from '../../../errors/ErrorResponse';

/**
 * Request to add a thing to a bulk
 * @validatable
 */
export type SCBulkAddRequest = SCThings;

/**
 * Route for indexing SC things in a bulk
 */
export class SCBulkAddRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCNotFoundErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.obligatoryParameters = {
      UID: 'SCUuid',
    };
    this.requestBodyName = 'SCBulkAddRequest';
    this.responseBodyName = 'SCBulkAddResponse';
    this.statusCodeSuccess = 201;
    this.urlFragment = '/bulk/:UID';
  }
}
