/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAbstractRoute, SCRouteHttpVerbs} from '../../../../Route';
import {
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse,
  SCNotFoundErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
  SCUnsupportedMediaTypeErrorResponse,
  SCValidationErrorResponse,
} from '../../../errors/ErrorResponse';

/**
 * Request to change the bulk state to done (close the bulk process)
 * @validatable
 */
export interface SCBulkDoneRequest {
}

/**
 * Route for closing bulks
 */
export class SCBulkDoneRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCNotFoundErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.obligatoryParameters = {
      UID: 'SCUuid',
    };
    this.requestBodyName = 'SCBulkDoneRequest';
    this.responseBodyName = 'SCBulkDoneResponse';
    this.statusCodeSuccess = 204;
    this.urlFragment = '/bulk/:UID/done';
  }
}
