/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';

/**
 * Types of payment that are accepted at a place.
 */
export type SCThingThatAcceptsPaymentsAcceptedPayments =
  'Cash'
  | 'Credit'
  | 'Cafeteria Card';

/**
 * A place without references that accepts payments
 */
export interface SCThingThatAcceptsPaymentsWithoutReferences extends SCThing {
  /**
   * Accepted payments of the place
   */
  paymentsAccepted?: SCThingThatAcceptsPaymentsAcceptedPayments[];
}

/**
 * Meta information about a thing without references that accepts payments
 */
export class SCThingThatAcceptsPaymentsWithoutReferencesMeta extends SCThingMeta implements
             SCMetaTranslations<SCThingThatAcceptsPaymentsWithoutReferences> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
    },
  };
}
