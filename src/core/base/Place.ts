/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta, SCThingTranslatableProperties} from '../Thing';
import {SCGeoInformation} from '../types/GeoInformation';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCPostalAddress} from '../types/PostalAddress';

/**
 * A place without references
 */
export interface SCPlaceWithoutReferences extends SCThing {
  /**
   * Address of the place
   */
  address?: SCPostalAddress;

  /**
   * Positional information of the place
   *
   * !!! BEWARE !!!
   * Can not be a GeometryCollection because ElasticSearch does not allow distance filtering/sorting on other types
   */
  geo: SCGeoInformation;

  /**
   * Opening hours of the place
   * @see http://wiki.openstreetmap.org/wiki/Key:opening_hours/specification
   */
  openingHours?: string;

  /**
   * Translated fields of a place
   */
  translations?: SCTranslations<SCPlaceWithoutReferencesTranslatableProperties>;

}

export interface SCPlaceWithoutReferencesTranslatableProperties extends SCThingTranslatableProperties {
  address?: SCPostalAddress;
}

/**
 * Meta information about creative works
 */
export class SCPlaceWithoutReferencesMeta extends SCThingMeta implements SCMetaTranslations<SCPlaceWithoutReferences> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
    },
  };
}
