/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing} from '../Thing';
import {SCThingMeta} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';

/**
 * An academic degree without references
 */
export interface SCAcademicDegreeWithoutReferences extends SCThing {
  /**
   * The achievable academic degree
   */
  academicDegree: SCGermanAcademicDegree;

  /**
   * The achievable academic degree with academic field specification 
   * (eg. Master of Science)
   */
  academicDegreewithField: string;

  /**
   * The achievable academic degree with academic field specification 
   * shorted (eg. M.Sc.).
   */
  academicDegreewithFieldShort: string;
}

export interface SCAcademicDegree extends SCAcademicDegreeWithoutReferences {
}

/**
 * Meta information about academic degrees
 */
export class SCAcademicDegreeMeta extends SCThingMeta implements SCMetaTranslations<SCAcademicDegree> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...SCThingMeta.getInstance().fieldTranslations.de,
      academicDegree: 'Hochschulgrad',
      academicDegreewithField: 'Abschlussbezeichnungen',
      academicDegreewithFieldShort: 'Abschlussbezeichnungen (kurz)',
    },
    en: {
      ...SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...SCThingMeta.getInstance().fieldValueTranslations.de,
      academicDegree: {
        'bachelor': 'Bachelor',
        'diploma': 'Diplom',
        'doctor': 'Doktor',
        'licentiate': 'Lizenziat',
        'magister': 'Magister',
        'master': 'Master',
        'masterstudent': 'Meisterschüler',
        'state examination': 'Staatsexamen',
      },
    },
    en: {
      ...SCThingMeta.getInstance().fieldValueTranslations.en,
    },
  };
}

/**
 * Types of (german) academic degrees
 */
export type SCGermanAcademicDegree = 'bachelor' |
                                     'diploma' |
                                     'doctor' |
                                     'licentiate' |
                                     'magister' |
                                     'master' |
                                     'master pupil' |
                                     'state examination' ;
