/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingType} from './Thing';
import {SCAcademicEvent, SCAcademicEventMeta, SCAcademicEventWithoutReferences} from './things/AcademicEvent';
import {SCArticle, SCArticleMeta, SCArticleWithoutReferences} from './things/Article';
import {SCBook, SCBookMeta, SCBookWithoutReferences} from './things/Book';
import {SCBuilding, SCBuildingMeta, SCBuildingWithoutReferences} from './things/Building';
import {SCCatalog, SCCatalogMeta, SCCatalogWithoutReferences} from './things/Catalog';
import {SCCourseOfStudies, SCCourseOfStudiesMeta, SCCourseOfStudiesWithoutReferences} from './things/CourseOfStudies';
import {SCDateSeries, SCDateSeriesMeta, SCDateSeriesWithoutReferences} from './things/DateSeries';
import {SCDiff, SCDiffMeta, SCDiffWithoutReferences} from './things/Diff';
import {SCDish, SCDishMeta, SCDishWithoutReferences} from './things/Dish';
import {SCFavorite, SCFavoriteMeta, SCFavoriteWithoutReferences} from './things/Favorite';
import {SCFloor, SCFloorMeta, SCFloorWithoutReferences} from './things/Floor';
import {SCMessage, SCMessageMeta, SCMessageWithoutReferences} from './things/Message';
import {SCOrganization, SCOrganizationMeta, SCOrganizationWithoutReferences} from './things/Organization';
import {SCPerson, SCPersonMeta, SCPersonWithoutReferences} from './things/Person';
import {SCPointOfInterest, SCPointOfInterestMeta, SCPointOfInterestWithoutReferences} from './things/PointOfInterest';
import {SCRoom, SCRoomMeta, SCRoomWithoutReferences} from './things/Room';
import {SCSemester, SCSemesterMeta, SCSemesterWithoutReferences} from './things/Semester';
import {SCSetting, SCSettingMeta, SCSettingWithoutReferences} from './things/Setting';
import {SCSportCourse, SCSportCourseMeta, SCSportCourseWithoutReferences} from './things/SportCourse';
import {SCStudyModule, SCStudyModuleMeta, SCStudyModuleWithoutReferences} from './things/StudyModule';
import {SCTicket, SCTicketMeta, SCTicketWithoutReferences} from './things/Ticket';
import {SCToDo, SCToDoMeta, SCToDoWithoutReferences} from './things/ToDo';
import {SCTour, SCTourMeta, SCTourWithoutReferences} from './things/Tour';
import {SCVideo, SCVideoMeta, SCVideoWithoutReferences} from './things/Video';

/* tslint:disable:variable-name */
/**
 * A map of things, from type to meta data
 */
export const SCClasses: { [K in SCThingType]: any } = {
  /* tslint:enable */
  'academic event': SCAcademicEventMeta,
  'article': SCArticleMeta,
  'book': SCBookMeta,
  'building': SCBuildingMeta,
  'catalog': SCCatalogMeta,
  'course of studies': SCCourseOfStudiesMeta,
  'date series': SCDateSeriesMeta,
  'diff': SCDiffMeta,
  'dish': SCDishMeta,
  'favorite': SCFavoriteMeta,
  'floor': SCFloorMeta,
  'message': SCMessageMeta,
  'organization': SCOrganizationMeta,
  'person': SCPersonMeta,
  'point of interest': SCPointOfInterestMeta,
  'room': SCRoomMeta,
  'semester': SCSemesterMeta,
  'setting': SCSettingMeta,
  'sport course': SCSportCourseMeta,
  'study module': SCStudyModuleMeta,
  'ticket': SCTicketMeta,
  'todo': SCToDoMeta,
  'tour': SCTourMeta,
  'video': SCVideoMeta,
};

export type SCThingsWithoutDiff =
  SCAcademicEvent
  | SCArticle
  | SCBook
  | SCBuilding
  | SCCatalog
  | SCCourseOfStudies
  | SCDateSeries
  | SCDish
  | SCFavorite
  | SCFloor
  | SCMessage
  | SCOrganization
  | SCPerson
  | SCPointOfInterest
  | SCRoom
  | SCSemester
  | SCSetting
  | SCSportCourse
  | SCStudyModule
  | SCTicket
  | SCToDo
  | SCTour
  | SCVideo;

/**
 * An object that exists in the StAppsCore
 */
export type SCThings =
  SCThingsWithoutDiff
  | SCDiff;

/**
 * A field of a thing
 */
export type SCThingsField = keyof SCThings | string;

/**
 * Thing without references for a thing
 */
export type SCAssociatedThingWithoutReferences<THING extends SCThings> =
  THING extends SCAcademicEvent ? SCAcademicEventWithoutReferences :
    THING extends SCArticle ? SCArticleWithoutReferences :
      THING extends SCBook ? SCBookWithoutReferences :
        THING extends SCBuilding ? SCBuildingWithoutReferences :
          THING extends SCCatalog ? SCCatalogWithoutReferences :
            THING extends SCCourseOfStudies ? SCCourseOfStudiesWithoutReferences :
              THING extends SCDateSeries ? SCDateSeriesWithoutReferences :
                THING extends SCDiff ? SCDiffWithoutReferences :
                  THING extends SCDish ? SCDishWithoutReferences :
                    THING extends SCFavorite ? SCFavoriteWithoutReferences :
                      THING extends SCFloor ? SCFloorWithoutReferences :
                        THING extends SCMessage ? SCMessageWithoutReferences :
                          THING extends SCOrganization ? SCOrganizationWithoutReferences :
                            THING extends SCPerson ? SCPersonWithoutReferences :
                              THING extends SCPointOfInterest ? SCPointOfInterestWithoutReferences :
                                THING extends SCRoom ? SCRoomWithoutReferences :
                                  THING extends SCSemester ? SCSemesterWithoutReferences :
                                    THING extends SCSetting ? SCSettingWithoutReferences :
                                      THING extends SCSportCourse ? SCSportCourseWithoutReferences :
                                        THING extends SCStudyModule ? SCStudyModuleWithoutReferences :
                                          THING extends SCTicket ? SCTicketWithoutReferences :
                                            THING extends SCToDo ? SCToDoWithoutReferences :
                                              THING extends SCTour ? SCTourWithoutReferences :
                                                THING extends SCVideo ? SCVideoWithoutReferences :
                                                  never;

/**
 * Thing for a thing without references
 */
export type SCAssociatedThing<THING extends SCThings> =
  THING extends SCAcademicEventWithoutReferences ? SCAcademicEvent :
    THING extends SCArticleWithoutReferences ? SCArticle :
      THING extends SCBookWithoutReferences ? SCBook :
        THING extends SCBuildingWithoutReferences ? SCBuilding :
          THING extends SCCatalogWithoutReferences ? SCCatalog :
            THING extends SCCourseOfStudiesWithoutReferences ? SCCourseOfStudies :
              THING extends SCDateSeriesWithoutReferences ? SCDateSeries :
                THING extends SCDiffWithoutReferences ? SCDiff :
                  THING extends SCDishWithoutReferences ? SCDish :
                    THING extends SCFavoriteWithoutReferences ? SCFavorite :
                      THING extends SCFloorWithoutReferences ? SCFloor :
                        THING extends SCMessageWithoutReferences ? SCMessage :
                          THING extends SCOrganizationWithoutReferences ? SCOrganization :
                            THING extends SCPersonWithoutReferences ? SCPerson :
                              THING extends SCPointOfInterestWithoutReferences ? SCPointOfInterest :
                                THING extends SCRoomWithoutReferences ? SCRoom :
                                  THING extends SCSemesterWithoutReferences ? SCSemester :
                                    THING extends SCSettingWithoutReferences ? SCSetting :
                                      THING extends SCSportCourseWithoutReferences ? SCSportCourse :
                                        THING extends SCStudyModuleWithoutReferences ? SCStudyModule :
                                          THING extends SCTicketWithoutReferences ? SCTicket :
                                            THING extends SCToDoWithoutReferences ? SCToDo :
                                              THING extends SCTourWithoutReferences ? SCTour :
                                                THING extends SCVideoWithoutReferences ? SCVideo :
                                                  never;
