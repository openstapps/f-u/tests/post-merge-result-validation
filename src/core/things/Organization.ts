/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingInPlace} from '../base/ThingInPlace';
import {SCThing, SCThingMeta, SCThingType} from '../Thing';

/**
 * An organization without references
 */
export interface SCOrganizationWithoutReferences extends SCThing {
  /**
   * Type of an organization
   */
  type: SCThingType.Organization;
}

/**
 * An organization
 *
 * @validatable
 */
export interface SCOrganization extends SCOrganizationWithoutReferences, SCThingInPlace {
  /**
   * Type of an organization
   */
  type: SCThingType.Organization;
}

/**
 * Meta information about an organization
 */
export class SCOrganizationMeta extends SCThingMeta {
}
