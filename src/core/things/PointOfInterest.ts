/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCPlaceWithoutReferences, SCPlaceWithoutReferencesMeta} from '../base/Place';
import {SCThingInPlace} from '../base/ThingInPlace';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';

/**
 * A point of interest without references
 */
export interface SCPointOfInterestWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<SCPointOfInterestCategories,
    SCThingWithCategoriesSpecificValues>,
    SCPlaceWithoutReferences {
  /**
   * Translated properties of a point of interest
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of a point of interest
   */
  type: SCThingType.PointOfInterest;
}

/**
 * A point of interest
 *
 * @validatable
 */
export interface SCPointOfInterest extends SCPointOfInterestWithoutReferences, SCThingInPlace {
  /**
   * Translated properties of a point of interest
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of a point of interest
   */
  type: SCThingType.PointOfInterest;
}

/**
 * Categories of a point of interest
 */
export type SCPointOfInterestCategories =
  'computer'
  | 'validator'
  | 'card charger'
  | 'printer'
  | 'disabled access';

/**
 * Meta information about points of interest
 */
export class SCPointOfInterestMeta extends SCThingMeta implements SCMetaTranslations<SCPointOfInterest> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      // tslint:disable-next-line:max-line-length
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCPointOfInterestCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.de,
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldTranslations.de,
    },
    en: {
      // tslint:disable-next-line:max-line-length
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCPointOfInterestCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.en,
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCPointOfInterestCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.de,
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldValueTranslations.de,
      type: 'Sonderziel',
    },
    en: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCPointOfInterestCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.en,
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.PointOfInterest,
    },
  };
}
