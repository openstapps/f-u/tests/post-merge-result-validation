/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAcademicTermWithoutReferences, SCAcademicTermWithoutReferencesMeta} from '../base/AcademicTerm';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';

/**
 * A semester without references
 */
export interface SCSemesterWithoutReferences extends SCAcademicTermWithoutReferences {
  /**
   * The short name of the semester, using the given pattern.
   *
   * @pattern ^(WS|SS) [0-9]{4}(/[0-9]{2})?$
   */
  acronym: string;

  /**
   * Type of the semester
   */
  type: SCThingType.Semester;
}

/**
 * A semester
 *
 * @validatable
 */
export interface SCSemester extends SCSemesterWithoutReferences {
  /**
   * Type of the semester
   */
  type: SCThingType.Semester;
}

/**
 * Meta information about a semester
 */
export class SCSemesterMeta extends SCThingMeta implements SCMetaTranslations<SCSemester> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCAcademicTermWithoutReferencesMeta.getInstance().fieldTranslations.de,
      acronym: 'Abkürzung',
      endDate: 'Ende',
      eventsEndDate: 'Vorlesungsschluss',
      eventsStartDate: 'Vorlesungsbeginn',
      startDate: 'Beginn',
    },
    en: {
      ... SCAcademicTermWithoutReferencesMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCAcademicTermWithoutReferencesMeta.getInstance().fieldValueTranslations.de,
      type: 'Semester',
    },
    en: {
      ... SCAcademicTermWithoutReferencesMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Semester,
    },
  };
}
