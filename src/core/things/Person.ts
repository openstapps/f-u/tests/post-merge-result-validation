/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCNationality} from '../types/i18n';
import {SCISO8601Date} from '../types/Time';
import {SCBuildingWithoutReferences} from './Building';
import {SCOrganizationWithoutReferences} from './Organization';
import {SCPointOfInterestWithoutReferences} from './PointOfInterest';
import {SCRoomWithoutReferences} from './Room';

/**
 * A person without references
 */
export interface SCPersonWithoutReferences extends SCThing {
  /**
   * Additional first names of the person.
   */
  additionalName?: string;

  /**
   * The birth date of the person.
   */
  birthDate?: SCISO8601Date;

  /**
   * The private email address of the person.
   *
   * @TJS-format email
   */
  email?: string;

  /**
   * The family name of the person.
   */
  familyName: string;

  /**
   * The private fax number of the person.
   */
  faxNumber?: string;

  /**
   * The gender of the person.
   */
  gender?: SCPersonGender;

  /**
   * The first name of the person.
   */
  givenName: string;

  /**
   * Honorific prefix of the person.
   */
  honorificPrefix?: string;

  /**
   * Honorific suffix of the person.
   */
  honorificSuffix?: string;

  /**
   * Titles of jobs that the person has.
   */
  jobTitles?: string[];

  /**
   * The complete name of the person combining all the parts of the name into one.
   */
  name: string;

  /**
   * The nationality of the person.
   */
  nationality?: SCNationality;

  /**
   * The private telephone number of the person.
   */
  telephone?: string;

  /**
   * Type of a person
   */
  type: SCThingType.Person;
}

/**
 * A person
 *
 * @validatable
 */
export interface SCPerson extends SCPersonWithoutReferences {
  /**
   * Organization the person works for
   */
  affiliations?: SCOrganizationWithoutReferences[];

  /**
   * A list of homes of the person
   */
  homeLocations?: Array<SCBuildingWithoutReferences
    | SCPointOfInterestWithoutReferences
    | SCRoomWithoutReferences>;

  /**
   * Type of a person
   */
  type: SCThingType.Person;

  /**
   * Locations where the person performs her/his work
   */
  workLocations?: SCContactPoint[];
}

/**
 * Meta information about a person
 */
export class SCPersonMeta extends SCThingMeta implements SCMetaTranslations<SCPerson> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
      type: 'Person',
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Person,
    },
  };
}

/**
 * A contact point
 *
 * @see http://schema.org/ContactPoint
 */
export interface SCContactPoint {
  /**
   * Exact place where work is performed
   */
  areaServed?: SCRoomWithoutReferences;

  /**
   * E-mail at the work location
   */
  email?: string;

  /**
   * Fax number at the work location
   */
  faxNumber?: string;

  /**
   * Times available for contacting at the work location
   */
  hoursAvailable?: string;

  /**
   * Contact number at the work location
   */
  telephone?: string;

  /**
   * URL at the work location
   */
  url?: string;
}

/**
 * Gender of a person
 */
export type SCPersonGender =
  'male'
  | 'female'
  | 'inter'
  | 'undefined';
