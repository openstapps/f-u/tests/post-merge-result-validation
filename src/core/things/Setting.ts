/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';

export type SCSettingCategories = string;

/**
 * A setting without references
 */
export interface SCSettingWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<SCSettingCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * The default value of a setting
   */
  defaultValue: SCSettingValue | SCSettingValues;
  /**
   * The input type of this setting
   */
  inputType: SCSettingInputType;
  /**
   * The order number this setting should show up in its category list
   */
  order: number;
  /**
   * Translated fields of a setting
   */
  translations?: SCTranslations<SCSettingValueTranslatableProperties>;
  /**
   * The type of this model
   */
  type: SCThingType.Setting;
  /**
   * The key of a value of a setting
   */
  value?: SCSettingValue | SCSettingValues;
  /**
   * The possible values of a setting
   */
  values?: SCSettingValues;
}

/**
 * The types of input/value a setting object can carry
 */
export enum SCSettingInputType {
  SingleChoice = 'single choice',
  MultipleChoice = 'multiple choice',
  Number = 'number',
  Text = 'text',
  Password = 'password',
}

/**
 * A setting with references
 *
 * @validatable
 */
export interface SCSetting extends SCSettingWithoutReferences {
  /**
   * The type of this model
   */
  type: SCThingType.Setting;
}

/**
 * The type a value of a setting can have
 */
export type SCSettingValue = string | number | boolean;

/**
 * The type of multiple values a setting can have
 */
export type SCSettingValues = SCSettingValue[];

/**
 * Translatable properties of a setting
 */
export interface SCSettingValueTranslatableProperties extends SCThingWithCategoriesTranslatableProperties {
  /**
   * The translations of the possible values of a setting
   */
  values?: string[];
}

/**
 * Meta information about settings
 */
export class SCSettingMeta extends SCThingMeta implements SCMetaTranslations<SCSetting> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      // tslint:disable-next-line:max-line-length
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCSettingCategories,
                SCThingWithCategoriesSpecificValues>().fieldTranslations.de,
      defaultValue: 'Standard Wert',
      inputType: 'Eingabetyp',
      value: 'Wert',
      values: 'Werte',
    },
    en: {
      // tslint:disable-next-line:max-line-length
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCSettingCategories,
                SCThingWithCategoriesSpecificValues>().fieldTranslations.en,
      defaultValue: 'default value',
      inputType: 'input type',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      // tslint:disable-next-line:max-line-length
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCSettingCategories,
                SCThingWithCategoriesSpecificValues>().fieldValueTranslations.de,
      inputType: {
        'multiple choice': 'mehrfach Auswahl',
        number: 'Zahl',
        password: 'Passwort',
        'single choice': 'einfache Auswahl',
        text: 'Text',
       },
      type: 'Einstellung',
    },
    en: {
      // tslint:disable-next-line:max-line-length
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCSettingCategories,
                SCThingWithCategoriesSpecificValues>().fieldValueTranslations.en,
      type: SCThingType.Setting,
    },
  };
}
