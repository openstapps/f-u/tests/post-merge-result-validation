/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingInPlace, SCThingInPlaceMeta} from '../base/ThingInPlace';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from '../base/ThingThatCanBeOffered';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCISO8601Date, SCISO8601Duration} from '../types/Time';
import {SCAcademicEventWithoutReferences} from './AcademicEvent';
import {SCPersonWithoutReferences} from './Person';
import {SCSportCourseWithoutReferences} from './SportCourse';

/**
 * Price groups for sport courses
 */
export interface SCSportCoursePriceGroup extends SCAcademicPriceGroup {
  /**
   * Price for alumnis
   */
  alumni?: number;
}

/**
 * A date without references
 */
export interface SCDateSeriesWithoutReferences extends SCThingThatCanBeOfferedWithoutReferences {
  /**
   * Dates of the date series that are initially planned to be held
   */
  dates: SCISO8601Date[];

  /**
   * Duration of the date series
   */
  duration: SCISO8601Duration;

  /**
   * Dates that where initially planned to be held but are cancelled
   */
  exceptions?: SCISO8601Date[];

  /**
   * Frequency of the date series
   */
  frequency: string;

  /**
   * Translated properties
   */
  translations?: SCTranslations<SCDateSeriesTranslatableProperties>;

  /**
   * Type of a date series
   */
  type: SCThingType.DateSeries;
}

/**
 * A date series
 *
 * @validatable
 */
export interface SCDateSeries extends SCDateSeriesWithoutReferences,
  SCThingInPlace,
  SCThingThatCanBeOffered<SCSportCoursePriceGroup> {
  /**
   * Event to which the date series belongs
   */
  event: SCAcademicEventWithoutReferences
    | SCSportCourseWithoutReferences;

  /**
   * Performers of the date series
   */
  performers?: SCPersonWithoutReferences[];

  /**
   * Translated properties
   */
  translations?: SCTranslations<SCDateSeriesTranslatableProperties>;

  /**
   * Type of a date series
   */
  type: SCThingType.DateSeries;
}

/**
 * Meta information about a date series
 */
export class SCDateSeriesMeta extends SCThingMeta implements SCMetaTranslations<SCDateSeries> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...SCThingInPlaceMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ...SCThingInPlaceMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...SCThingInPlaceMeta.getInstance().fieldValueTranslations.de,
      dates: 'Einzeltermine',
      duration: 'Dauer',
      frequency: 'Wiederholung',
      type: 'Wiederholungreihe',
    },
    en: {
      ...SCThingInPlaceMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.DateSeries,
    },
  };
}

/**
 * Translatable properties of date series'
 */
export interface SCDateSeriesTranslatableProperties
  extends SCThingThatCanBeOfferedTranslatableProperties {
  frequency?: string;
}
