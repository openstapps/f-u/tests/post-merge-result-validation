/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCPlaceWithoutReferences, SCPlaceWithoutReferencesMeta} from '../base/Place';
import {SCThingInPlace, SCThingInPlaceMeta} from '../base/ThingInPlace';
import {
  SCThingThatAcceptsPaymentsWithoutReferences,
  SCThingThatAcceptsPaymentsWithoutReferencesMeta,
} from '../base/ThingThatAcceptsPayments';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from '../base/ThingWithCategories';

import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCMap} from '../types/Map';

/**
 * Categories of a room
 */
export type SCRoomCategories =
  'cafe'
  | 'canteen'
  | 'computer'
  | 'education'
  | 'laboratory'
  | 'learn'
  | 'library'
  | 'lounge'
  | 'office'
  | 'restaurant'
  | 'restroom'
  | 'student canteen'
  | 'student union';

/**
 * A room without references
 */
export interface SCRoomWithoutReferences
  extends SCPlaceWithoutReferences, SCThingThatAcceptsPaymentsWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCRoomCategories, SCRoomSpecificValues> {
  /**
   * The name of the floor in which the room is in.
   */
  floor?: string;

  /**
   * The inventory of the place/room as a list of items and their quantity.
   */
  inventory?: SCMap<number>;

  /**
   * Translations of specific values of the object
   *
   * Take precedence over "main" value for selected languages.
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of the room
   */
  type: SCThingType.Room;
}

/**
 * A room
 *
 * @validatable
 */
export interface SCRoom extends SCRoomWithoutReferences, SCThingInPlace {
  /**
   * Translations of specific values of the object
   *
   * Take precedence over "main" value for selected languages.
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of the room
   */
  type: SCThingType.Room;
}

/**
 * Category specific values of a room
 */
export interface SCRoomSpecificValues extends SCThingWithCategoriesSpecificValues {
  /**
   * Category specific opening hours of the room
   */
  openingHours?: string;
}

/**
 * Meta information about a place
 */
export class SCRoomMeta extends SCThingMeta implements SCMetaTranslations<SCRoom> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldTranslations.de,
      ... SCThingThatAcceptsPaymentsWithoutReferencesMeta.getInstance().fieldTranslations.de,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCRoomCategories,
            SCRoomSpecificValues>().fieldTranslations.de,
      ... SCThingInPlaceMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldTranslations.en,
      ... SCThingThatAcceptsPaymentsWithoutReferencesMeta.getInstance().fieldTranslations.en,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCRoomCategories,
            SCRoomSpecificValues>().fieldTranslations.en,
      ... SCThingInPlaceMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldValueTranslations.de,
      ... SCThingThatAcceptsPaymentsWithoutReferencesMeta.getInstance().fieldValueTranslations.de,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCRoomCategories,
                SCRoomSpecificValues>().fieldValueTranslations.de,
      ... SCThingInPlaceMeta.getInstance().fieldValueTranslations.de,
      categories: {
        'cafe': 'Café',
        'canteen': 'Kantine',
        'computer': 'PC-Pool',
        'education': 'Bildung',
        'laboratory': 'Labor',
        'learn': 'Lernen',
        'library': 'Bibliothek',
        'lounge': 'Lounge',
        'office': 'Büro',
        'restaurant': 'Restaurant',
        'restroom': 'Toilette',
        'student canteen': 'Mensa',
        'student union': 'Studentenvereinigung',
      },
      type: 'Raum',
    },
    en: {
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldValueTranslations.en,
      ... SCThingThatAcceptsPaymentsWithoutReferencesMeta.getInstance().fieldValueTranslations.en,
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCRoomCategories,
                SCRoomSpecificValues>().fieldValueTranslations.en,
      ... SCThingInPlaceMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Room,
    },
  };
}
