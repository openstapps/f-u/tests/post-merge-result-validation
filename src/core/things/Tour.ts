/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';

/**
 * A tour without references
 */
export interface SCTourWithoutReferences extends SCThing {
  /**
   * Init script for the tour
   */
  init?: string;

  /**
   * Steps of a tour
   */
  steps: SCTourStep[];

  /**
   * Type of a tour
   */
  type: SCThingType.Tour;
}

/**
 * A tour
 *
 * @validatable
 */
export interface SCTour extends SCTourWithoutReferences {
  /**
   * Type of a tour
   */
  type: SCThingType.Tour;
}

/**
 * Meta information about a tour
 */
export class SCTourMeta extends SCThingMeta implements SCMetaTranslations<SCTour> {
}

/**
 * A step in a tour
 */
export type SCTourStep =
  SCTourStepMenu
  | SCTourStepLocation
  | SCTourStepTooltip;

/**
 * A location of a tour step
 */
export interface SCTourStepLocation {
  /**
   * Location to go to
   */
  location: string;

  /**
   * Type of the step
   */
  type: 'location';
}

/**
 * A tooltip in a tour step
 */
export interface SCTourStepTooltip {
  /**
   * Whether the tooltip may fail or not
   */
  canFail?: boolean;

  /**
   * Element that the tooltip shall be pointing at or a list of elements to try in the specified order
   */
  element: string | string[];

  /**
   * Position of the tooltip
   */
  position?: 'bottom' | 'left' | 'right' | 'top';

  /**
   * How the step shall be resolved
   */
  resolved?: SCTourResolvedElement | SCTourResolvedEvent | SCTourResolvedLocation | SCTourResolvedMenu;

  /**
   * Text that the tooltip shall contain
   */
  text: string;

  /**
   * How often it shall be retried
   */
  tries?: number;

  /**
   * Type of the step
   */
  type: 'tooltip';
}

/**
 * Menu interaction of a tour step
 */
export interface SCTourStepMenu {
  /**
   * The action that needs to be completed on the menu
   */
  action: 'close';

  /**
   * The side of the menu that the step refers to
   */
  side: 'right';

  /**
   * The type of the step
   */
  type: 'menu';
}

/**
 * Tour step resolved by an element
 */
export interface SCTourResolvedElement {
  /**
   * Element name
   */
  element: string;
}

/**
 * Tour step resolved by an event
 */
export interface SCTourResolvedEvent {
  /**
   * Event name
   */
  event: string;
}

/**
 * Tour step resolved by a location
 */
export interface SCTourResolvedLocation {
  /**
   * Matching location
   */
  location: SCTourResolvedLocationTypeIs | SCTourResolvedLocationTypeMatch;
}

/**
 * Tour step resolved by a location for a specific location
 */
export interface SCTourResolvedLocationTypeIs {
  /**
   * Specific location name
   */
  is: string;
}

/**
 * Tour step resolved by a location for a specific location
 */
export interface SCTourResolvedLocationTypeMatch {
  /**
   * Regex location name
   */
  match: string;
}

/**
 * Tour step resolved by interacting with a menu
 */
export interface SCTourResolvedMenu {
  /**
   * Menu location
   */
  menu: 'open-left' | 'open-right';
}
