## What needs to be remodeled?

??? - Describe use case!

## How is the current model not sufficient?

???

## Which changes are necessary?

???

## Do the proposed changes impact current use cases?

???

/label ~feature ~meeting
