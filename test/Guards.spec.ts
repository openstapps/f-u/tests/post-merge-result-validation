/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {slow, suite, test, timeout} from 'mocha-typescript';
import {SCThingOriginType, SCThingType} from '../src/core/Thing';
import {SCDish} from '../src/core/things/Dish';
import {isThing} from '../src/core/types/Guards';

@suite(timeout(10000), slow(5000))
export class GuardsSpec {
  @test
  public isThing() {
    const notADish = {
      categories: [
        'appetizer',
      ],
      name: 'foo',
      origin: {
        created: '',
        type: SCThingOriginType.User,
      },
      type: 'foobar',
      uid: 'bar',
    };

    const dish: SCDish = {
      categories: [
        'appetizer',
      ],
      name: 'foo',
      origin: {
        created: '',
        type: SCThingOriginType.User,
      },
      type: SCThingType.Dish,
      uid: 'bar',
    };

    expect(isThing('foo')).to.be.equal(false);

    expect(isThing(notADish)).to.be.equal(false);

    expect(isThing(dish)).to.be.equal(true);
  }
}
