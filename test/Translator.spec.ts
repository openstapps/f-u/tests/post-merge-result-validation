/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {slow, suite, test, timeout} from 'mocha-typescript';
import {SCThingOriginType, SCThingType} from '../src/core/Thing';
import {SCBuildingWithoutReferences} from '../src/core/things/Building';
import {SCDish, SCDishMeta} from '../src/core/things/Dish';
import {SCPerson} from '../src/core/things/Person';
import {SCThingTranslator} from '../src/core/Translator';

const building: SCBuildingWithoutReferences = {
  address: {
    addressCountry : 'base-address.addressCountry',
    addressLocality: 'base-address.addressLocality',
    postalCode: 'base-address.postalCode',
    streetAddress: 'base-address.streetAddress',
  },
  categories: ['office', 'education'],
  floors: ['base-floor0', 'base-floor1'],
  geo: {
    point: {
      coordinates: [12.0, 13.0],
      type: 'Point',
    },
  },
  name: 'base-space-name',
  origin: {
    indexed: '1970-01-01T00:00:00.000Z',
    name: 'building-connector',
    type: SCThingOriginType.Remote,
  },
  translations: {
    de: {
      address: {
        addressCountry : 'de-address.addressCountry',
        addressLocality: 'de-address.addressLocality',
        postalCode: 'de-address.postalCode',
        streetAddress: 'de-address.streetAddress',
      },
      floors: ['de-floor0', 'de-floor1'],
      name: 'de-space-name',
    },
  },
  type: SCThingType.Building,
  uid: '540862f3-ea30-5b8f-8678-56b4dc217140',

};

const dish: SCDish = {
  categories: ['main dish', 'dessert'],
  characteristics: [{name: 'base-characteristic0'}, {name: 'base-characteristic1'}],
  name: 'base-dish-name',
  offers: [
    {
    availability: 'in stock',
    inPlace: building,
    prices: {
      default: 23.42,
    },
    provider: {
      name: 'base-provider',
      origin: {
        indexed: '1970-01-01T00:00:00.000Z',
        name: 'dish-connector',
        type: SCThingOriginType.Remote,
        },
      type: SCThingType.Organization,
      uid: '540862f3-ea30-5b8f-8678-56b4dc217141',
      },
    },
  ],
  origin: {
    indexed: '1970-01-01T00:00:00.000Z',
    name: 'dish-connector',
    type: SCThingOriginType.Remote,
  },
  translations: {
    de: {
      characteristics: [{name: 'de-characteristic0'}, {name: 'de-characteristic1'}],
      name: 'de-dish-name',
    },
  },
  type: SCThingType.Dish,
  uid: '540862f3-ea30-5b8f-8678-56b4dc217140',
};

const person: SCPerson = {
  familyName: 'base-familyName-name',
  givenName: 'base-givenName-name',
  homeLocations: [building, building, building],
  name : 'base-person-name',
  origin: {
    indexed: '1970',
    name: 'ding',
    type: SCThingOriginType.Remote,
  },
  type: SCThingType.Person,
  uid: '1234',
};

const translator = new SCThingTranslator('de', 'en');
// tslint:disable-next-line:no-eval
const languageNonExistant = eval("'jp'");
// this will simulate a translator always utilizing the base language translations
const translatorWithFallback = new SCThingTranslator(languageNonExistant);

// tslint:disable:member-ordering TranslationSpec
@suite(timeout(10000), slow(5000))
export class TranslationSpec {

  @test
  public directStringLiteralType() {
    expect(translator.translate(dish).type()).to.equal('Essen');
  }

  @test
  public directStringProperty() {
    expect(translator.translate(dish).name()).to.equal('de-dish-name');
  }

  @test
  public directArrayOfString() {
    expect(translator.translate(dish).characteristics()).to.deep
    .equal([{name: 'de-characteristic0'}, {name: 'de-characteristic1'}]);
  }

  @test
  public directArrayOfStringSubscript() {
    expect(translator.translate(dish).characteristics[1]()).to.deep.equal({name: 'de-characteristic1'});
  }

  @test
  public directMetaArrayOfString() {
    expect(translator.translate(dish).categories()).to.deep.equal(['Hauptgericht', 'Nachtisch']);
  }

  @test
  public directMetaArrayOfStringSubscript() {
    expect(translator.translate(dish).categories[1]()).to.equal('Nachtisch');
  }

  @test
  public nestedStringLiteralType() {
    expect(translator.translate(dish).offers[0].inPlace.type()).to.equal('Gebäude');
  }

  @test
  public nestedStringProperty() {
    expect(translator.translate(dish).offers[0].inPlace.name()).to.equal('de-space-name');
  }

  @test
  public nestedMetaArrayOfString() {
    expect(translator.translate(dish).offers[0].inPlace.categories()).to.deep.equal(['Büro', 'Bildung']);
  }

  @test
  public nestedMetaArrayOfStringSubscript() {
    expect(translator.translate(dish).offers[0].inPlace.categories[1]()).to.equal('Bildung');
  }

  @test
  public directStringLiteralTypeFallback() {
    expect(translatorWithFallback.translate(dish).type()).to.equal('dish');
  }

  @test
  public directStringPropertyFallback() {
    expect(translatorWithFallback.translate(dish).name()).to.equal('base-dish-name');
  }

  @test
  public directArrayOfStringSubscriptFallback() {
    expect(translatorWithFallback.translate(dish).characteristics[1]())
    .to.deep.equal({name: 'base-characteristic1'});
  }

  @test
  public directMetaArrayOfStringFallback() {
    expect(translatorWithFallback.translate(dish).categories()).to.deep.equal(['main dish', 'dessert']);
  }

  @test
  public directMetaArrayOfStringSubscriptFallback() {
    expect(translatorWithFallback.translate(dish).categories[1]()).to.equal('dessert');
  }

  @test
  public nestedStringLiteralTypeFallback() {
    expect(translatorWithFallback.translate(dish).offers[0].inPlace.type()).to.equal('building');
  }

  @test
  public nestedStringPropertyFallback() {
    expect(translatorWithFallback.translate(dish).offers[0].inPlace.name()).to.equal('base-space-name');
  }

  @test
  public nestedMetaArrayOfStringFallback() {
    expect(translatorWithFallback.translate(dish).offers[0].inPlace.categories())
    .to.deep.equal(['office', 'education']);
  }

  @test
  public nestedMetaArrayOfStringSubscriptFallback() {
    expect(translatorWithFallback.translate(dish).offers[0].inPlace.categories[1]()).to.equal('education');
  }

  @test
  public directStringLiteralTypeUndefined() {
    // tslint:disable-next-line:no-eval
    const undefinedThing = eval('(x) => undefined;');
    expect(translator.translate(undefinedThing())('defaultValue')).to.equal('defaultValue');
    expect(translator.translate(dish).name('defaultValue')).to.not.equal('defaultValue');
  }

  @test
  public nestedMetaArrayOfStringSubscriptUndefined() {
    expect(translator.translate(dish).offers[0].inPlace.categories[1234]('printer')).to.equal('printer');
    expect(translator.translate(dish).offers[0].inPlace.categories[1]('printer')).to.not.equal('printer');
  }
}

// tslint:disable:member-ordering TranslationSpecByString
@suite(timeout(10000), slow(5000))
export class TranslationSpecByString {

  @test
  public directStringLiteralType() {
    expect(translator.getFieldValueTranslation(dish, 'type')).to.equal('Essen');
  }

  @test
  public directStringProperty() {
    expect(translator.getFieldValueTranslation(dish, 'name')).to.equal('de-dish-name');
  }

  @test
  public directArrayOfString() {
    expect(translator.getFieldValueTranslation(dish, 'characteristics')).to.deep
    .equal([{name: 'de-characteristic0'}, {name: 'de-characteristic1'}]);
  }

  @test
  public directArrayOfStringSubscript() {
    expect(translator.getFieldValueTranslation(dish, 'characteristics[1]'))
    .to.deep.equal({name: 'de-characteristic1'});
  }

  @test
  public directMetaArrayOfString() {
    expect(translator.getFieldValueTranslation(dish, 'categories')).to.deep.equal(['Hauptgericht', 'Nachtisch']);
  }

  @test
  public directMetaArrayOfStringSubscript() {
    expect(translator.getFieldValueTranslation(dish, 'categories[1]')).to.equal('Nachtisch');
  }

  @test
  public nestedStringLiteralType() {
    expect(translator.getFieldValueTranslation(dish, 'offers[0].inPlace.type')).to.equal('Gebäude');
  }

  @test
  public nestedStringProperty() {
    expect(translator.getFieldValueTranslation(dish, 'offers[0].inPlace.name')).to.equal('de-space-name');
  }

  @test
  public nestedMetaArrayOfString() {
    expect(translator.getFieldValueTranslation(dish, 'offers[0].inPlace.categories'))
    .to.deep.equal(['Büro', 'Bildung']);
  }

  @test
  public nestedMetaArrayOfStringSubscript() {
    expect(translator.getFieldValueTranslation(dish, 'offers[0].inPlace.categories[1]')).to.equal('Bildung');
  }

  @test
  public nestedArrayOfStringSubscript() {
    expect(translator.getFieldValueTranslation(dish, 'offers[0].inPlace.floors[1]')).to.equal('de-floor1');
  }

  @test
  public directStringLiteralTypeFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'type')).to.equal('dish');
  }

  @test
  public directStringPropertyFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'name')).to.equal('base-dish-name');
  }

  @test
  public directArrayOfStringSubscriptFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'characteristics[1]'))
    .to.deep.equal({name: 'base-characteristic1'});
  }

  @test
  public directMetaArrayOfStringFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'categories'))
    .to.deep.equal(['main dish', 'dessert']);
  }

  @test
  public directMetaArrayOfStringSubscriptFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'categories[1]')).to.equal('dessert');
  }

  @test
  public nestedStringLiteralTypeFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'offers[0].inPlace.type')).to.equal('building');
  }

  @test
  public nestedStringPropertyFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'offers[0].inPlace.name')).to.equal('base-space-name');
  }

  @test
  public nestedMetaArrayOfStringFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'offers[0].inPlace.categories'))
    .to.deep.equal(['office', 'education']);
  }

  @test
  public nestedMetaArrayOfStringSubscriptFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'offers[0].inPlace.categories[1]'))
    .to.equal('education');
  }

  @test
  public nestedArrayOfStringSubscriptFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'offers[0].inPlace.floors[1]'))
    .to.equal('base-floor1');
  }

  @test
  public nestedArrayOfStringSubscriptUncommonFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(dish, 'offers[0].inPlace.floors.1')).to.equal('base-floor1');
  }

  @test
  public nestedNestedMetaArrayOfStringSubscriptUncommonFallback() {
    expect(translatorWithFallback.getFieldValueTranslation(person, 'homeLocations.1.categories.1'))
    .to.equal('education');
  }
}

// tslint:disable:member-ordering no-eval no-unused-expression TranslationSpec
@suite(timeout(10000), slow(5000))
export class MetaTranslationSpec {

  @test
  public consistencyWithMetaClass() {
    const dishMetaTranslationsDE = translator.translatedPropertyNames(dish);
    const dishMetaTranslationsEN = translator.translatedPropertyNames(dish, 'en');
    expect(dishMetaTranslationsDE).to.deep.equal(SCDishMeta.getInstance().fieldTranslations.de);
    expect(dishMetaTranslationsEN).to.deep.equal(SCDishMeta.getInstance().fieldTranslations.en);
  }

  @test
  public thingWithoutMetaClass() {
    const dishCopy = Object.assign({}, dish);
    const typeNonExistant = eval("(x) => x + 'typeNonExistant';");
    // this will assign a non existant SCThingType to dishCopy
    dishCopy.type = typeNonExistant();
    const dishMetaTranslationsDE = translator.translatedPropertyNames(dishCopy);
    expect(dishMetaTranslationsDE).to.be.undefined;
  }
}
