/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {slow, suite, test, timeout} from 'mocha-typescript';
import {SCBulkRoute} from '../src/core/protocol/routes/bulk/BulkRequest';
import {SCBulkAddRoute} from '../src/core/protocol/routes/bulk/UID/BulkAddRequest';
import {SCThingUpdateRoute} from '../src/core/protocol/routes/TYPE/UID/ThingUpdateRequest';

@suite(timeout(10000), slow(5000))
export class RoutesSpec {
  @test
  public bulkAddRouteUrlFragment() {
    const bulkAddRoute = new SCBulkAddRoute();

    expect(bulkAddRoute.getUrlFragment({
      UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
    })).to.equal('/bulk/540862f3-ea30-5b8f-8678-56b4dc217140');
  }

  @test
  public bulkRouteUrlFragment() {
    const bulkRoute = new SCBulkRoute();

    expect(bulkRoute.getUrlFragment()).to.equal('/bulk');
  }

  @test
  public thingUpdateRouteUrlFragment() {
    const thingUpdateRoute = new SCThingUpdateRoute();

    expect(thingUpdateRoute.getUrlFragment({
      TYPE: 'dish',
      UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
    })).to.equal('/dish/540862f3-ea30-5b8f-8678-56b4dc217140');
  }

  @test
  public tooManyParameters() {
    const thingUpdateRoute = new SCThingUpdateRoute();

    const fn = () => {
      thingUpdateRoute.getUrlFragment({
        FOO: 'bar',
        TYPE: 'dish',
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      });
    };

    expect(fn).to.throw('Extraneous parameters provided.');
  }

  @test
  public wrongParameters() {
    const thingUpdateRoute = new SCThingUpdateRoute();

    const fn = () => {
      thingUpdateRoute.getUrlFragment({
        TYPO: 'dish',
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      });
    };

    expect(fn).to.throw('Parameter \'TYPE\' not provided.');
  }
}
